package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pxelit.admin.zoeinventorymanagement.BAL.Customer;
import com.pxelit.admin.zoeinventorymanagement.R;

import java.util.List;

/**
 * Created by Admin on 17/10/2016.
 */

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {
    private List<Customer> CustomersRouteList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView lblCustomerName;
        public EditText _txtTotalSales;

        public MyViewHolder(View view) {
            super(view);
            lblCustomerName = (TextView) view.findViewById(R.id.lblCustomer);
            _txtTotalSales = (EditText) view.findViewById(R.id.txtTotalSales);
        }
    }

    public CustomerAdapter(List<Customer> CustomerList) {
        this.CustomersRouteList = CustomerList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_list_route_today, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Customer customer = CustomersRouteList.get(position);
        holder.lblCustomerName.setText("Nudier Mena1");
        holder._txtTotalSales.setText("109876");

    }

    @Override
    public int getItemCount() {
        return CustomersRouteList.size();
    }
}
