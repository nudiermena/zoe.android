package com.pxelit.admin.zoeinventorymanagement;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pxelit.admin.zoeinventorymanagement.Common.Constans;
import com.pxelit.admin.zoeinventorymanagement.Common.InputNameDialogFragment;
import com.pxelit.admin.zoeinventorymanagement.Common.JsonParser;
import com.pxelit.admin.zoeinventorymanagement.DAL.DataAdapter;
import com.pxelit.admin.zoeinventorymanagement.Model.Response;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends FragmentActivity implements InputNameDialogFragment.InputNameDialogListener {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Button btnSkip, btnNext;
    private PrefManager prefManager;
    private Button _btnSaveAndCreateDatabase;
    DataAdapter db;
    ProgressDialog _progressDialog;
    List<GetDataAsync> _tasks;
    Spinner _sItemstoDownload;

    private void StartAsyncTaskInParallel(String dataToSync, String tag) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new GetDataAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constans.SYNCSERVICEURL, dataToSync, tag);
        } else {
            new GetDataAsync().execute(Constans.SYNCSERVICEURL, dataToSync, tag);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void SyncEmployee(View view) {

//        new GetDataAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constans.SYNCSERVICEURL, Constans.SYNCEMPLOYEE);
//        new GetDataAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constans.SYNCSERVICEURL, Constans.SYNCSALESREP);
//        new GetDataAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constans.SYNCSERVICEURL, Constans.SYNCCUSTOMERMSG);
//        new GetDataAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Constans.SYNCSERVICEURL, Constans.SYNCTERM);

        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCTERM, "SYNCTERM");
        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCCUSTOMERMSG, "SYNCCUSTOMERMSG");
        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCSALESREP, "SYNCSALESREP");
        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCEMPLOYEE, "SYNCEMPLOYEE");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_progressDialog != null) {
            _progressDialog.dismiss();
            _progressDialog = null;
        }
    }

    public void SyncInvoice(View view) {
        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCTAXES);
    }

    public void SyncTerms(View view) {
        new GetDataAsync().execute(Constans.SYNCSERVICEURL, Constans.SYNCTERM);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        _tasks = new ArrayList<>();

        this._sItemstoDownload = new Spinner(this);


//        prefManager = new PrefManager(this);
//        if (!prefManager.isFirstTimeLaunch()) {
//            launchHomeScreen();
//            finish();
//        }

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }


        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        //btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);


        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.activity_welcome1,
                R.layout.activity_welcome2};

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

//        btnSkip.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                launchHomeScreen();
//            }
//        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
        //prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
        finish();
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                btnNext.setText(getString(R.string.start));
                //btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                btnNext.setText(getString(R.string.next));
                //btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void showInputNameDialog() {
        android.app.FragmentManager fragmentManager = getFragmentManager();
        InputNameDialogFragment inputNameDialog = new InputNameDialogFragment();
        inputNameDialog.setCancelable(false);
        inputNameDialog.setDialogTitle("Enter Name");
        inputNameDialog.show(fragmentManager, "input dialog");
    }


    public void btnShowDialog(View view) {
        showInputNameDialog();
    }

    public void CopyDB(InputStream inputStream, OutputStream outputStream) throws IOException {
        //---copy 1K bytes at a time--
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, length);
        }
        inputStream.close();
        outputStream.close();
    }


    private Boolean CreateDataBase() {
        Boolean dbCreated = false;
        final String destDir = "/data/data/" + getPackageName() + "/databases/";
        String destPath = destDir + "zoe";
        File f = new File(destPath);
        //if (f.exists()) {
        //---make sure directory exists--
        File directory = new File(destDir);
        directory.mkdirs();

        //---copy the db from the assets folder into
        // the databases folder--
        try {
            CopyDB(getBaseContext().getAssets().open("zoe"), new FileOutputStream(destPath));
            dbCreated = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //}
        db = new DataAdapter(this);
        return dbCreated;
    }

    @Override
    public void onFinishInputDialog(String inputText) {
        if (inputText.toUpperCase().equals("CONFIRM")) {
            Snackbar.make(findViewById(R.id.cordinatorLayout), "Data base created sucessfuly", Snackbar.LENGTH_SHORT).show();
            //Toast.makeText(this, "Data base created sucessfuly", Toast.LENGTH_SHORT).show();
            if (this.CreateDataBase()) {
                this.btnNext.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private class GetDataAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            _progressDialog = new ProgressDialog(WelcomeActivity.this);
            _progressDialog.setTitle("Synchronizing");
            _progressDialog.setMessage("One moment please...");
            _progressDialog.setCancelable(true);
            _progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    GetDataAsync.this.cancel(true);
                    _progressDialog.dismiss();
                }
            });
            _progressDialog.show();
            if (_tasks.size() == 0) {

            }
            _tasks.add(this);
        }

        private void ProcessCustomer(Response response) {
            com.pxelit.admin.zoeinventorymanagement.BAL.Customer customer = new com.pxelit.admin.zoeinventorymanagement.BAL.Customer();

            try {
                customer.InsertarQBXML(getApplicationContext(), response);
            } catch (Exception e) {
                Log.d("InsertarQBXML", e.getMessage());
            }
        }

        private void ProcessCustomerMsg(Response response) {
            com.pxelit.admin.zoeinventorymanagement.BAL.CustomerMsg customerMsg = new com.pxelit.admin.zoeinventorymanagement.BAL.CustomerMsg();

            try {
                customerMsg.InsertarQBXML(getApplicationContext(), response);
            } catch (Exception e) {
                Log.d("ProcessCustomerMsg", e.getMessage());
            }

        }

        private void ProcessSalesRep(Response response) {
            com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep();

            try {
                salesRep.InsertarQBXML(getApplicationContext(), response);
            } catch (Exception e) {
                Log.d("InsertarQBXML", e.getMessage());
            }
        }

        private void UpdateEmployee(Response response) {
            com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep();
            try {
                salesRep.UpdateQBXML(getApplicationContext(), response);
            } catch (Exception ex) {
                Log.d("UpdateEmployee", ex.getMessage());
            }
        }

        private void ProcessJson(String json, String tag) {
            Gson gson = new Gson();
            Response response = gson.fromJson(json, Response.class);
            if (tag == "SYNCEMPLOYEE") {
                ProcessSalesRep(response);
                UpdateEmployee(response);
            } else if (tag == "SYNCTERM") {
                ProcessTerm(response);
            } else if (tag == "SYNCSALESREP") {
                ProcessCustomerMsg(response);
            }
        }

        private void ProcessTerm(Response response) {
            com.pxelit.admin.zoeinventorymanagement.BAL.Terms terms = new com.pxelit.admin.zoeinventorymanagement.BAL.Terms();
            try {
                terms.InsertarQBXML(getApplicationContext(), response);
            } catch (Exception e) {
                Log.d("InsertarQBXML", e.getMessage());
            }
        }

        @Override
        protected void onPostExecute(String data) {
            String[] qq = data.split("#");
            String json = qq[0];
            String tag = qq[1];
            Gson gson = new Gson();
            Response response = gson.fromJson(json, Response.class);
            if (tag.equals("SYNCEMPLOYEE")) {
                ProcessSalesRep(response);
                UpdateEmployee(response);
            } else if (tag.equals("SYNCTERM")) {
                ProcessTerm(response);
            } else if (tag.equals("SYNCCUSTOMERMSG")) {
                ProcessCustomerMsg(response);
            } else if (tag.equals("SYNCEMPLOYEE")) {
                ProcessCustomer(response);
            }
//            Gson gson = new Gson();
//            Response response = gson.fromJson(json, Response.class);
//            ProcessSalesRep(response);
//            UpdateEmployee(response);
//            ProcessCustomerMsg(response);

            _tasks.remove(this);
            if (_tasks.size() == 0) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {
            String uri = params[0];
            String tag = params[2];
            String cc;

            String j = JsonParser.getJSONFromUrl(uri, params[1]);
            int leng = j.length();
            try {
                j = j.substring(1, leng - 2);
                //ProcessJson(j, tag);
            } catch (Exception ex) {
                Log.d("doInBackground", ex.getMessage());
            }
            cc = j + "#" + tag;
            return cc;
        }
    }
}