package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 14/11/2016.
 */

public class CustomerRet {
    public String get_ListID() {
        return ListID;
    }

    public void set_ListID(String _ListID) {
        this.ListID = _ListID;
    }

    public String get_TimeCreated() {
        return TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this.TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this.TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this.EditSequence = _EditSequence;
    }

    public String get_Name() {
        return Name;
    }

    public void set_Name(String _Name) {
        this.Name = _Name;
    }

    public String get_FullName() {
        return FullName;
    }

    public void set_FullName(String _FullName) {
        this.FullName = _FullName;
    }

    public Integer get_IsActive() {
        return IsActive;
    }

    public void set_IsActive(Integer _IsActive) {
        this.IsActive = _IsActive;
    }

    public String get_Sublevel() {
        return Sublevel;
    }

    public String get_CompanyName() {
        return CompanyName;
    }

    public void set_CompanyName(String _CompanyName) {
        this.CompanyName = _CompanyName;
    }

    public String get_Phone() {
        return Phone;
    }

    public void set_Phone(String _Phone) {
        this.Phone = _Phone;
    }

    public String get_Fax() {
        return Fax;
    }

    public void set_Fax(String _Fax) {
        this.Fax = _Fax;
    }

    public String get_Email() {
        return Email;
    }

    public void set_Email(String _Email) {
        this.Email = _Email;
    }

    public String get_Contact() {
        return Contact;
    }

    public void set_Contact(String _Contact) {
        this.Contact = _Contact;
    }

    public String get_AltContact() {
        return AltContact;
    }

    public void set_AltContact(String _AltContact) {
        this.AltContact = _AltContact;
    }

    public Number get_Balance() {
        return Balance;
    }

    public void set_Balance(Number _Balance) {
        this.Balance = _Balance;
    }

    public String get_TotalBalance() {
        return TotalBalance;
    }

    public void set_TotalBalance(String _TotalBalance) {
        this.TotalBalance = _TotalBalance;
    }

    public String get_JobStatus() {
        return JobStatus;
    }

    public void set_JobStatus(String _JobStatus) {
        this.JobStatus = _JobStatus;
    }

    public String get_PreferredDeliveryMethod() {
        return PreferredDeliveryMethod;
    }

    public void set_PreferredDeliveryMethod(String _PreferredDeliveryMethod) {
        this.PreferredDeliveryMethod = _PreferredDeliveryMethod;
    }

    private String ListID;
    private String TimeCreated;
    private String TimeModified;
    private String EditSequence;
    private String Name;
    private String FullName;
    private Integer IsActive;
    private String Sublevel;
    private String CompanyName;
    private String Phone;
    private String Fax;
    private String Email;
    private String Contact;
    private String AltContact;
    private Number Balance;
    private String TotalBalance;
    private String JobStatus;
    private String PreferredDeliveryMethod;
    public TermsRef TermsRef;
    public SalesRepRef SalesRepRef;
    public BillAddress BillAddress;
    public BillAddressBlock BillAddressBlock;
    public ShipAddress shipAddress;
    public ShipAddressBlock ShipAddressBlock;
    public ShipToAddress ShipToAddress;
    public List<DataExtRet> DataExtRet;
}
