package com.pxelit.admin.zoeinventorymanagement.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pxelit.admin.zoeinventorymanagement.*;

/**
 * Created by Admin on 14/10/2016.
 */

public class ConfigFragment extends android.app.Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.config_fragment, null);
        return rootView;
    }
}
