package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

import java.security.PublicKey;

/**
 * Created by Admin on 10/11/2016.
 */

public class SalesRep {

    DataAdapter _dataAdapter;

    public Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep salesRepBAL) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        //contentValues.put("id_salesrep", salesRepBAL.get_salesrepID());
        contentValues.put("Name", salesRepBAL.get_name());
        contentValues.put("Password", salesRepBAL.get_password());
        contentValues.put("isActive", salesRepBAL.get_isActive());
        contentValues.put("SyncTime", salesRepBAL.get_syncTime());

        Long rowsAffected = this._dataAdapter.ExecuteNonQueryInsert(Constans.SALESREPTABLENAME, contentValues);
        this._dataAdapter.close();
        return rowsAffected;
    }

    public Integer Update(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.SalesRep salesRepBAL) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String query = "Name = " + salesRepBAL.get_name();
        ContentValues contentValues = new ContentValues();
        contentValues.put("initial", salesRepBAL.get_initial());
        Integer rowsAffected = this._dataAdapter.ExecuteNonQueryUpdate(Constans.SALESREPTABLENAME, contentValues, query);
        this._dataAdapter.close();
        return rowsAffected;
    }

    public Cursor Exists(Context ctx, String userName, String password) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String[] columsToRetrive = {"Name", "Password"};
        String query = "Name =" + "'" + userName + "'" + " and " + "Password =" + "'" + password + "'";
        Cursor cursor = this._dataAdapter.ExecuteDataDatable(Constans.SALESREPTABLENAME, columsToRetrive, query);
        //this._dataAdapter.close();
        return cursor;

    }

//    public Cursor GetByID() {
//        Cursor mCursor = db.query(true, "", new String[]{KEY_SERIAL, KEY_NOMBREYAPELLIDO, KEY_TELEFONOGPS,
//                KEY_PLACA, KEY_TELEFONOPROPIETARIO, KEY_CONTRASENA}, KEY_SERIAL + "=" + rowId, null, null, null, null, null);
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//        return mCursor;
//
//    }

    public Cursor GetAll(Context ctx) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String[] columsToRetrive = {"id_salesrep", "Name", "Password", "isActive"};
        Cursor cursor = this._dataAdapter.ExecuteDataDatable(Constans.SALESREPTABLENAME, columsToRetrive, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        }
        this._dataAdapter.close();
        return cursor;
    }
}
