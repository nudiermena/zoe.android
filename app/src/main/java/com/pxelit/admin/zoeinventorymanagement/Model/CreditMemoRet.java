package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 15/11/2016.
 */

public class CreditMemoRet {
    public String get_TxnID() {
        return _TxnID;
    }

    public void set_TxnID(String _TxnID) {
        this._TxnID = _TxnID;
    }

    public Date get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(Date _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public Date get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(Date _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_TxnNumber() {
        return _TxnNumber;
    }

    public void set_TxnNumber(String _TxnNumber) {
        this._TxnNumber = _TxnNumber;
    }

    public String get_TxnDate() {
        return _TxnDate;
    }

    public void set_TxnDate(String _TxnDate) {
        this._TxnDate = _TxnDate;
    }

    public String get_RefNumber() {
        return _RefNumber;
    }

    public void set_RefNumber(String _RefNumber) {
        this._RefNumber = _RefNumber;
    }

    public Boolean get_IsPending() {
        return _IsPending;
    }

    public void set_IsPending(Boolean _IsPending) {
        this._IsPending = _IsPending;
    }

    public String get_PONumber() {
        return _PONumber;
    }

    public void set_PONumber(String _PONumber) {
        this._PONumber = _PONumber;
    }

    public String get_DueDate() {
        return _DueDate;
    }

    public void set_DueDate(String _DueDate) {
        this._DueDate = _DueDate;
    }

    public String get_ShipDate() {
        return _ShipDate;
    }

    public void set_ShipDate(String _ShipDate) {
        this._ShipDate = _ShipDate;
    }

    public String get_Subtotal() {
        return _Subtotal;
    }

    public void set_Subtotal(String _Subtotal) {
        this._Subtotal = _Subtotal;
    }

    public String get_SalesTaxPercentage() {
        return _SalesTaxPercentage;
    }

    public void set_SalesTaxPercentage(String _SalesTaxPercentage) {
        this._SalesTaxPercentage = _SalesTaxPercentage;
    }

    public String get_SalesTaxTotal() {
        return _SalesTaxTotal;
    }

    public void set_SalesTaxTotal(String _Email) {
        this._SalesTaxTotal = _SalesTaxTotal;
    }

    public String get_TotalAmount() {
        return _TotalAmount;
    }

    public void set_TotalAmount(String _TotalAmount) {
        this._TotalAmount = _TotalAmount;
    }

    public String get_CreditRemaining() {
        return _CreditRemaining;
    }

    public void set_CreditRemaining(String _CreditRemaining) {
        this._CreditRemaining = _CreditRemaining;
    }

    public String get_Memo() {
        return _Memo;
    }

    public void set_Memo(String _Memo) {
        this._Memo = _Memo;
    }

    public Boolean get_IsToBePrinted() {
        return _IsToBePrinted;
    }

    public void set_IsToBePrinted(Boolean _IsToBePrinted) {
        this._IsToBePrinted = _IsToBePrinted;
    }

    public Boolean get_IsToBeEmailed() {
        return _IsToBeEmailed;
    }

    public void set_IsToBeEmailed(Boolean _IsToBeEmailed) {
        this._IsToBeEmailed = _IsToBeEmailed;
    }

    private String _TxnID;
    private Date _TimeCreated;
    private Date _TimeModified;
    private String _EditSequence;
    private String _TxnNumber;

    public CustomerRef CustomerRef;
    public ARAccountRef ARAccountRef;
    public TemplateRef TemplateRef;

    private String _TxnDate;
    private String _RefNumber;

    public BillAddress BillAddress;
    public BillAddressBlock BillAddressBlock;
    public ShipAddress ShipAddress;
    public ShipAddressBlock ShipAddressBlock;

    private Boolean _IsPending;
    private String _PONumber;

    public List<TermsRef> TermsRef;

    private String _DueDate;

    public SalesRepRef SalesRepRef;

    private String _ShipDate;
    private String _Subtotal;
    private String _SalesTaxPercentage;
    private String _SalesTaxTotal;
    private String _TotalAmount;
    private String _CreditRemaining;
    private String _Memo;

    //public List<CustomerMsgRef> CustomerMsgRef;

    private Boolean _IsToBePrinted;
    private Boolean _IsToBeEmailed;

    //public List<LinkedTxn> LinkedTxn;
    public CreditMemoLineRet CreditMemoLineRet;
}
