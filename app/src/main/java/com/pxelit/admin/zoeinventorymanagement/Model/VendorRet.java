package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class VendorRet {
    public String get_ListID() {
        return _ListID;
    }

    public void set_ListID(String _ListID) {
        this._ListID = _ListID;
    }

    public String get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public Boolean get_IsActive() {
        return _IsActive;
    }

    public void set_IsActive(Boolean _IsActive) {
        this._IsActive = _IsActive;
    }

    public String get_CompanyName() {
        return _CompanyName;
    }

    public void set_CompanyName(String _CompanyName) {
        this._CompanyName = _CompanyName;
    }

    public String get_Phone() {
        return _Phone;
    }

    public void set_Phone(String _Phone) {
        this._Phone = _Phone;
    }

    public String get_Fax() {
        return _Fax;
    }

    public void set_Fax(String _Fax) {
        this._Fax = _Fax;
    }

    public String get_NameOnCheck() {
        return _NameOnCheck;
    }

    public void set_NameOnCheck(String _NameOnCheck) {
        this._NameOnCheck = _NameOnCheck;
    }

    public Boolean get_IsVendorEligibleFor1099() {
        return _IsVendorEligibleFor1099;
    }

    public void set_IsVendorEligibleFor1099(Boolean _IsVendorEligibleFor1099) {
        this._IsVendorEligibleFor1099 = _IsVendorEligibleFor1099;
    }

    public String get_Balance() {
        return _Balance;
    }

    public void set_Balance(String _Balance) {
        this._Balance = _Balance;
    }

    private String _ListID;
    private String _TimeCreated;
    private String _TimeModified;
    private String _EditSequence;
    private String _Name;
    private Boolean _IsActive;
    private String _CompanyName;
    private String _Fax;

    List<VendorAddress> VendorAddress;
    List<VendorAddressBlock> VendorAddressBlock;

    private String _Phone;
    List<AdditionalContactRef> AdditionalContactRef;

    private String _NameOnCheck;
    private Boolean _IsVendorEligibleFor1099;
    private String _Balance;
}
