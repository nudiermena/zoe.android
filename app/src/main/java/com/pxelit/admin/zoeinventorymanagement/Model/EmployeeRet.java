package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 10/11/2016.
 */

public class EmployeeRet {
    String ListID;
    Date TimeCreated;

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public Date getTimeCreated() {
        return TimeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        TimeCreated = timeCreated;
    }

    public Date getTimeModified() {
        return TimeModified;
    }

    public void setTimeModified(Date timeModified) {
        TimeModified = timeModified;
    }

    public Long getEditSequence() {
        return EditSequence;
    }

    public void setEditSequence(Long editSequence) {
        EditSequence = editSequence;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getPrintAs() {
        return PrintAs;
    }

    public void setPrintAs(String printAs) {
        PrintAs = printAs;
    }

    public Long getPhone() {
        return Phone;
    }

    public void setPhone(Long phone) {
        Phone = phone;
    }

    public String getEmployeeType() {
        return EmployeeType;
    }

    public void setEmployeeType(String employeeType) {
        EmployeeType = employeeType;
    }

    Date TimeModified;
    Long EditSequence;
    String Name;
    Boolean IsActive;
    String FirstName;
    String LastName;
    String PrintAs;
    Long Phone;
    String EmployeeType;

    public EmployeeAddress EmployeeAddress;
    public AdditionalContactRef AdditionalContactRef;
    public List<DataExtRet> DataExtRet;
}

