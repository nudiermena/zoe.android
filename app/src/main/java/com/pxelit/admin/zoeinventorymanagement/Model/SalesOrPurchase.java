package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 16/11/2016.
 */

public class SalesOrPurchase {

    public String get_Price() {
        return _Price;
    }

    public void set_Price(String _Price) {
        this._Price = _Price;
    }

    public String get_Desc() {
        return _Desc;
    }

    public void set_Desc(String _Desc) {
        this._Desc = _Desc;
    }

    private String _Price;
    private String _Desc;
    List<AccountRef> AccountRef;
}
