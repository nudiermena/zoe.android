package com.pxelit.admin.zoeinventorymanagement;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.pxelit.admin.zoeinventorymanagement.BAL.Customer;
import com.pxelit.admin.zoeinventorymanagement.Common.Constans;
import com.pxelit.admin.zoeinventorymanagement.DAL.CustomerAdapter;
import com.pxelit.admin.zoeinventorymanagement.Fragments.InventoryFragment;
import com.pxelit.admin.zoeinventorymanagement.Fragments.InvoiceFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private android.app.Fragment fragment;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    List<String> listDataHeader;
    Toolbar toolbar;
    List<Customer> customers = new ArrayList<Customer>();
    CustomerAdapter customerAdapter;
    TextView _lblUserName;
    SharedPreferences sharedPreferences;
    final String prefName = "zoe_sharedpreference";
    private Handler mHandler;

    private static String FILE = "c:/temp/FirstPdf.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);


//    public void ReplaceFragment(android.app.Fragment fragment) {
//        if (fragment == null)
//            return;
//
//        FragmentManager fragmentManager = getFragmentManager();
//        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.content_frame, fragment);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
//    }

    public void FillDropCustomerInvoice() {


    }


    public String ReadSharedValues(String Key) {
        sharedPreferences = getSharedPreferences(prefName, MODE_PRIVATE);
        return sharedPreferences.getString(Key, "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mHandler = new Handler();

        String data = ReadSharedValues(Constans.SALESPREFNAME);

        PrefManager prefManager = new PrefManager(this);
        prefManager.setFirstTimeLaunch(true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle =
                new ActionBarDrawerToggle(this, this.mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        this.mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View v = navigationView.getHeaderView(0);
        this._lblUserName = (TextView) v.findViewById(R.id.lblUserName);
        this._lblUserName.setText(data);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        } else {
            super.onBackPressed();
        }
    }

    private AdapterView.OnItemClickListener mDrawerItemClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            switch (position) {
                case 0:
                    // fragment = new MercuryFragment();
                    break;
                case 1:
                    //fragment = new VenusFragment();
                    break;
                case 2:
                    //fragment = new EarthFragment();
                    break;
                default:
                    return;
            }

            //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerLayout.closeDrawer(mDrawerList);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);

    }

    private void LoadHomeFragments(final MenuItem item) {
        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment(item);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.content_frame, fragment, null);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        //toggleFab();

        //Closing drawer on item click
        this.mDrawerLayout.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_invoice:
                // invoice
                InvoiceFragment invoiceFragment = new InvoiceFragment();
                return invoiceFragment;
            case R.id.nav_inventory_transfer:
                //inventorySite
                InventoryFragment inventoryFragment = new InventoryFragment();
                return inventoryFragment;
            default:
                return new InventoryFragment();
        }
    }

    // iText allows to add metadata to the PDF which can be viewed in your Adobe
    // Reader
    // under File -> Properties
    private static void addMetaData(Document document) {
        document.addTitle("Invoice / CM");
        document.addSubject("Invoice / CM");
        document.addAuthor("");
        document.addCreator("");
    }

    private static void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        preface.add(new Paragraph("Invoice / CM", subFont));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Andress vendor:", catFont));
        preface.add(new Paragraph("City, Estado, Zipcode", catFont));
        preface.add(new Paragraph("Telefono, Fax", catFont));
        preface.add(new Paragraph("Sitio web", catFont));
        preface.add(new Paragraph("------------------------------", catFont));
        addEmptyLine(preface, 1);

        preface.add(new Paragraph("Nombre cliente"));
        preface.add(new Paragraph("Adress cliente"));
        preface.add(new Paragraph("City | Estado | Zipcode"));
        preface.add(new Paragraph("Telefono | Fax"));
        preface.add(new Paragraph("------------------------------", catFont));
        addEmptyLine(preface, 1);

        preface.add(new Paragraph("Fecha", smallBold));
        preface.add(new Paragraph("Referencia", smallBold));
        preface.add(new Paragraph("Sales rep", smallBold));
        preface.add(new Paragraph("------------------------------", catFont));
        addEmptyLine(preface, 1);
        document.add(preface);

    }

    private static void createTable(Section subCatPart)
            throws BadElementException {
        PdfPTable table = new PdfPTable(4);

        PdfPCell c1 = new PdfPCell(new Phrase("Qty"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Product"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Price"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Amount"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        table.setHeaderRows(1);

        table.addCell("1.0");
        table.addCell("1.1");
        table.addCell("1.2");
        table.addCell("2.1");
        table.addCell("2.2");
        table.addCell("2.3");
        subCatPart.add(table);
    }


    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }


    private void CreatePDf() {
        String FILE = Environment.getExternalStorageDirectory().toString()
                + "/PDF/" + "Name.pdf";

        // Create New Blank Document
        Document document = new Document(PageSize.A4);

        // Create Directory in External Storage
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/PDF");
        myDir.mkdirs();

        // Create Pdf Writer for Writting into New Created Document
        try {
            PdfWriter.getInstance(document, new FileOutputStream(FILE));

            // Open Document for Writting into document
            document.open();

            // User Define Method
            addMetaData(document);
            addTitlePage(document);


        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (DocumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // Close Document after writting all content
        document.close();

        Toast.makeText(this, "PDF File is Created. Location : " + FILE,
                Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //this.mDrawerLayout.closeDrawer(GravityCompat.START);
        LoadHomeFragments(item);
        return true;
    }


}
