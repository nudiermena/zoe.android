package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 19/11/2016.
 */

public class InvoiceRet {

    public String get_TxnID() {
        return _TxnID;
    }

    public void set_TxnID(String _TxnID) {
        this._TxnID = _TxnID;
    }

    public String get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_TxnNumber() {
        return _TxnNumber;
    }

    public void set_TxnNumber(String _TxnNumber) {
        this._TxnNumber = _TxnNumber;
    }

    public String get_TxnDate() {
        return _TxnDate;
    }

    public void set_TxnDate(String _TxnDate) {
        this._TxnDate = _TxnDate;
    }

    public String get_RefNumber() {
        return _RefNumber;
    }

    public void set_RefNumber(String _RefNumber) {
        this._RefNumber = _RefNumber;
    }

    public Boolean get_IsPending() {
        return _IsPending;
    }

    public void set_IsPending(Boolean _IsPending) {
        this._IsPending = _IsPending;
    }

    public Boolean get_IsFinanceCharge() {
        return _IsFinanceCharge;
    }

    public void set_IsFinanceCharge(Boolean _IsFinanceCharge) {
        this._IsFinanceCharge = _IsFinanceCharge;
    }

    public String get_PONumber() {
        return _PONumber;
    }

    public void set_PONumber(String _PONumber) {
        this._PONumber = _PONumber;
    }

    public String get_DueDate() {
        return _DueDate;
    }

    public void set_DueDate(String _DueDate) {
        this._DueDate = _DueDate;
    }

    public String get_ShipDate() {
        return _ShipDate;
    }

    public void set_ShipDate(String _ShipDate) {
        this._ShipDate = _ShipDate;
    }

    public String get_Subtotal() {
        return _Subtotal;
    }

    public void set_Subtotal(String _Subtotal) {
        this._Subtotal = _Subtotal;
    }

    public String get_SalesTaxPercentage() {
        return _SalesTaxPercentage;
    }

    public void set_SalesTaxPercentage(String _SalesTaxPercentage) {
        this._SalesTaxPercentage = _SalesTaxPercentage;
    }

    public String get_SalesTaxTotal() {
        return _SalesTaxTotal;
    }

    public void set_SalesTaxTotal(String _Email) {
        this._SalesTaxTotal = _SalesTaxTotal;
    }

    public String get_TotalAmount() {
        return _TotalAmount;
    }

    public void set_TotalAmount(String _TotalAmount) {
        this._TotalAmount = _TotalAmount;
    }

    public String get_BalanceRemaining() {
        return _BalanceRemaining;
    }

    public void set_BalanceRemaining(String _BalanceRemaining) {
        this._BalanceRemaining = _BalanceRemaining;
    }

    public String get_Memo() {
        return _Memo;
    }

    public void set_Memo(String _Memo) {
        this._Memo = _Memo;
    }

    public Boolean get_IsPaid() {
        return _IsPaid;
    }

    public void set_IsPaid(Boolean _IsPaid) {
        this._IsPaid = _IsPaid;
    }

    public Boolean get_IsToBePrinted() {
        return _IsToBePrinted;
    }

    public void set_IsToBePrinted(Boolean _IsToBePrinted) {
        this._IsToBePrinted = _IsToBePrinted;
    }

    public Boolean get_IsToBeEmailed() {
        return _IsToBeEmailed;
    }

    public void set_IsToBeEmailed(Boolean _IsToBeEmailed) {
        this._IsToBeEmailed = _IsToBeEmailed;
    }

    private String _TxnID;
    private String _TimeCreated;
    private String _TimeModified;
    private String _EditSequence;
    private String _TxnNumber;

    List<CustomerRef> CustomerRef;
    List<ClassRef> ClassRef;
    List<ARAccountRef> ARAccountRef;
    List<TemplateRef> TemplateRef;

    private String _TxnDate;
    private String _RefNumber;

    List<BillAddress> BillAddress;
    List<BillAddressBlock> BillAddressBlock;
    List<ShipAddress> ShipAddress;
    List<ShipAddressBlock> ShipAddressBlock;

    private Boolean _IsPending;
    private Boolean _IsFinanceCharge;
    private String _PONumber;

    List<TermsRef> TermsRef;

    private String _DueDate;

    List<SalesRepRef> SalesRepRef;

    private String _ShipDate;
    private String _Subtotal;
    private String _SalesTaxPercentage;
    private String _SalesTaxTotal;
    private String _TotalAmount;
    private String _BalanceRemaining;
    private String _Memo;
    private Boolean _IsPaid;

    List<CustomerMsgRef> CustomerMsgRef;

    private Boolean _IsToBePrinted;
    private Boolean _IsToBeEmailed;

    List<InvoiceLineRet> InvoiceLineRet;
}
