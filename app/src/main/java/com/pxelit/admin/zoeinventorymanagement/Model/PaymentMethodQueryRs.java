package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class PaymentMethodQueryRs {

    public String get_requestID() {
        return _requestID;
    }

    public void set_requestID(String _requestID) {
        this._requestID = _requestID;
    }

    public Integer get_statusCode() {
        return _statusCode;
    }

    public void set_statusCode(Integer _statusCode) {
        this._statusCode = _statusCode;
    }

    public String get_statusSeverity() {
        return _statusSeverity;
    }

    public void set_statusSeverity(String _statusSeverity) {
        this._statusSeverity = _statusSeverity;
    }

    public String get_statusMessage() {
        return _statusMessage;
    }

    public void set_statusMessage(String _statusMessage) {
        this._statusMessage = _statusMessage;
    }

    private String _requestID;
    private Integer _statusCode;
    private String _statusSeverity;
    private String _statusMessage;

    public List<PaymentMethodRet> PaymentMethodRet;
}
