package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

/**
 * Created by Admin on 12/11/2016.
 */

public class Terms {

    public static Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.Terms termBAL) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        contentValues.put("name", termBAL.get_name());
        contentValues.put("stdDueDays", termBAL.get_stdDueDay());
        contentValues.put("stdDiscountDays", termBAL.get_stdDiscountDays());
        contentValues.put("discountPct", termBAL.get_discountPct());
        Long rowsAffected = _dataAdapter.ExecuteNonQueryInsert(Constans.TERM_TABLENAME, contentValues);
        _dataAdapter.close();
        return rowsAffected;
    }

//    public Cursor GetByID() {
//        Cursor mCursor = db.query(true, "", new String[]{KEY_SERIAL, KEY_NOMBREYAPELLIDO, KEY_TELEFONOGPS,
//                KEY_PLACA, KEY_TELEFONOPROPIETARIO, KEY_CONTRASENA}, KEY_SERIAL + "=" + rowId, null, null, null, null, null);
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//        return mCursor;
//
//    }

    public static Cursor GetAll(Context ctx) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();
        String sql = "select _id, name, stdDueDays, stdDiscountDays , discountPct from term";
        Cursor cursor = _dataAdapter.ExecuteDataRawQuery(sql, null);
//        if (cursor != null) {
//            if (cursor.moveToFirst()) {
//            }
//        }
        //_dataAdapter.close();
        return cursor;
    }

}
