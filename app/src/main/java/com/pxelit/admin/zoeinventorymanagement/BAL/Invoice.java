package com.pxelit.admin.zoeinventorymanagement.BAL;

/**
 * Created by Admin on 17/10/2016.
 */

public class Invoice {

    private String _id;
    private String _listID;
    private String _po_number;
    private String _txnDate;
    private String _dueDate;
    private Number _appliedAmount;
    private Number get_appliedAmount;
    private String _billAddress_addr1;
    private String _billAddress_addr2;
    private String _billAddress_city;
    private String _billAddress_state;
    private String _billAddress_postalcode;
    private String _shipAddress_addr1;
    private String _shipAddress_addr2;
    private String _shipAddress_city;
    private String _shipAddress_state;
    private String _shipAddress_postalcode;
    private Boolean _isPaid;
    private Boolean _isPending;
    private String _refNumber;
    private Number _salesTaxPercentage;
    private Number _salesTaxTotal;
    private String _shipDate;
    private Number _subtotal;
    private String _termID;
    private String _billAddress_addr3;
    private String _customerMsg_ListID;
    private String _shipAddress_addr3;
    private String _memo;
    private String _zoeUpdateDate;
    private String _zoeSycDate;
    private Boolean _needSync;
    private String _origin;
    private String _salesrepID;
    private String _signature;
    private String _photo;
    private String _signaturePNG;
    private Boolean _needCorrection;
}
