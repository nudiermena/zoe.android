package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Model.CustomerMsgRet;
import com.pxelit.admin.zoeinventorymanagement.Model.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 23/11/2016.
 */

public class CustomerMsg {

    private String ListID;
    private String FullName;

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }


    public void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.CustomerMsg customerMsg = new com.pxelit.admin.zoeinventorymanagement.DAL.CustomerMsg();
        CustomerMsg CustomerMsgBAL = new CustomerMsg();
        CustomerMsgRet _customerMsgRet;

        List<CustomerMsgRet> customerMsgRets = response.QBXML.QBXMLMsgsRs.CustomerMsgQueryRs.CustomerMsgRet;
        if (customerMsgRets == null)
            return;

        for (Iterator<CustomerMsgRet> c = customerMsgRets.iterator(); c.hasNext(); ) {
            _customerMsgRet = c.next();
            CustomerMsgBAL.ListID = _customerMsgRet.get_ListID();
            CustomerMsgBAL.FullName = _customerMsgRet.get_Name();
            com.pxelit.admin.zoeinventorymanagement.DAL.CustomerMsg.Insert(ctx, CustomerMsgBAL);
        }
    }

    public List<String> GetAll(Context ctx) {
        List<String> labels = new ArrayList<String>();
        com.pxelit.admin.zoeinventorymanagement.DAL.CustomerMsg customerMsgDAL = new com.pxelit.admin.zoeinventorymanagement.DAL.CustomerMsg();
        Cursor cursor = customerMsgDAL.GetAll(ctx);
        if (!cursor.isClosed()) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return labels;
    }
}
