package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;

/**
 * Created by Admin on 12/11/2016.
 */

public class PaymentMethodRet {
    public String get_ListID() {
        return _ListID;
    }

    public void set_ListID(String _ListID) {
        this._ListID = _ListID;
    }

    public String get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public Boolean get_IsActive() {
        return _IsActive;
    }

    public void set_IsActive(Boolean _IsActive) {
        this._IsActive = _IsActive;
    }

    public String get_PaymentMethodType() {
        return _PaymentMethodType;
    }

    public void set_PaymentMethodType(String _PaymentMethodType) {
        this._PaymentMethodType = _PaymentMethodType;
    }

    private String _ListID;
    private String _TimeCreated;
    private String _TimeModified;
    private String _EditSequence;
    private String _Name;
    private Boolean _IsActive;
    private String _PaymentMethodType;
}
