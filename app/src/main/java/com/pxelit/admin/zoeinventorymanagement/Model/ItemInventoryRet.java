package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 16/11/2016.
 */

public class ItemInventoryRet {

    public String get_ListID() {
        return _ListID;
    }

    public void set_ListID(String _ListID) {
        this._ListID = _ListID;
    }

    public String get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public String get_FullName() {
        return _FullName;
    }

    public void set_FullName(String _FullName) {
        this._FullName = _FullName;
    }

    public Boolean get_IsActive() {
        return _IsActive;
    }

    public void set_IsActive(Boolean _IsActive) {
        this._IsActive = _IsActive;
    }

    public String get_Sublevel() {
        return _Sublevel;
    }

    public void set_Sublevel(String _Sublevel) {
        this._Sublevel = _Sublevel;
    }

    public String get_SalesDesc() {
        return _SalesDesc;
    }

    public void set_SalesDesc(String _SalesDesc) {
        this._SalesDesc = _SalesDesc;
    }

    public String get_SalesPrice() {
        return _SalesPrice;
    }

    public void set_SalesPrice(String _SalesPrice) {
        this._SalesPrice = _SalesPrice;
    }

    public String get_PurchaseDesc() {
        return _PurchaseDesc;
    }

    public void set_PurchaseDesc(String _PurchaseDesc) {
        this._PurchaseDesc = _PurchaseDesc;
    }

    public String get_PurchaseCost() {
        return _PurchaseCost;
    }

    public void set_PurchaseCost(String _PurchaseCost) {
        this._PurchaseCost = _PurchaseCost;
    }

    public String get_QuantityOnHand() {
        return _QuantityOnHand;
    }

    public void set_QuantityOnHand(String _QuantityOnHand) {
        this._QuantityOnHand = _QuantityOnHand;
    }

    public String get_AverageCost() {
        return _AverageCost;
    }

    public void set_AverageCost(String _AverageCost) {
        this._AverageCost = _AverageCost;
    }

    public String get_QuantityOnOrder() {
        return _QuantityOnOrder;
    }

    public void set_QuantityOnOrder(String _QuantityOnOrder) {
        this._QuantityOnOrder = _QuantityOnOrder;
    }

    public String get_QuantityOnSalesOrder() {
        return _QuantityOnSalesOrder;
    }

    public void set_QuantityOnSalesOrder(String _QuantityOnSalesOrder) {
        this._QuantityOnSalesOrder = _QuantityOnSalesOrder;
    }

    private String _ListID;
    private String _TimeCreated;
    private String _TimeModified;
    private String _EditSequence;
    private String _Name;
    private String _FullName;
    private Boolean _IsActive;
    private String _Sublevel;

    List<SalesTaxCodeRef> SalesTaxCodeRef;

    private String _SalesDesc;
    private String _SalesPrice;

    List<IncomeAccountRef> IncomeAccountRef;

    private String _PurchaseDesc;
    private String _PurchaseCost;

    List<COGSAccountRef> COGSAccountRef;
    List<PrefVendorRef> PrefVendorRef;
    List<AssetAccountRef> AssetAccountRef;

    private String _QuantityOnHand;
    private String _AverageCost;
    private String _QuantityOnOrder;
    private String _QuantityOnSalesOrder;

}
