package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class CustomerMsgQueryRs {
    public String get_requestID() {
        return requestID;
    }

    public void set_requestID(String _requestID) {
        this.requestID = _requestID;
    }

    public Integer get_statusCode() {
        return statusCode;
    }

    public void set_statusCode(Integer _statusCode) {
        this.statusCode = _statusCode;
    }

    public String get_statusSeverity() {
        return statusSeverity;
    }

    public void set_statusSeverity(String _statusSeverity) {
        this.statusSeverity = _statusSeverity;
    }

    public String get_statusMessage() {
        return statusMessage;
    }

    public void set_statusMessage(String _statusMessage) {
        this.statusMessage = _statusMessage;
    }

    String requestID;
    Integer statusCode;
    String statusSeverity;
    String statusMessage;
    public List<CustomerMsgRet> CustomerMsgRet;
}
