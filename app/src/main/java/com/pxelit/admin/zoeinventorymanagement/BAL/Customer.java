package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Model.CustomerQueryRs;
import com.pxelit.admin.zoeinventorymanagement.Model.CustomerRet;
import com.pxelit.admin.zoeinventorymanagement.Model.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 17/10/2016.
 */

public class Customer {

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public Integer getIsActive() {
        return IsActive;
    }

    public void setIsActive(Integer isActive) {
        IsActive = isActive;
    }

    public String getBillAddress1() {
        return BillAddress1;
    }

    public void setBillAddress1(String billAddress1) {
        BillAddress1 = billAddress1;
    }

    public String getBillAddress2() {
        return BillAddress2;
    }

    public void setBillAddress2(String billAddress2) {
        BillAddress2 = billAddress2;
    }

    public String getShipAddress1() {
        return ShipAddress1;
    }

    public void setShipAddress1(String shipAddress1) {
        ShipAddress1 = shipAddress1;
    }

    public String getShipAddress2() {
        return ShipAddress2;
    }

    public void setShipAddress2(String shipAddress2) {
        ShipAddress2 = shipAddress2;
    }

    public Number getOpenBalance() {
        return OpenBalance;
    }

    public void setOpenBalance(Number openBalance) {
        OpenBalance = openBalance;
    }

    public Number getOverdueBalance() {
        return OverdueBalance;
    }

    public void setOverdueBalance(Number overdueBalance) {
        OverdueBalance = overdueBalance;
    }

    public String getWorkPhone() {
        return WorkPhone;
    }

    public void setWorkPhone(String workPhone) {
        WorkPhone = workPhone;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String cellPhone) {
        CellPhone = cellPhone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    private String FullName;
    private Integer IsActive;
    private String BillAddress1;
    private String BillAddress2;
    private String ShipAddress1;
    private String ShipAddress2;
    private Number OpenBalance;
    private Number OverdueBalance;
    private String WorkPhone;
    private String CellPhone;
    private String Email;

    public String getShipAddressZipcode() {
        return ShipAddressZipcode;
    }

    public void setShipAddressZipcode(String shipAddressZipcode) {
        ShipAddressZipcode = shipAddressZipcode;
    }

    public String getBillAddresZipCode() {
        return BillAddresZipCode;
    }

    public void setBillAddresZipCode(String billAddresZipCode) {
        BillAddresZipCode = billAddresZipCode;
    }

    public String getBillAddresCity() {
        return BillAddresCity;
    }

    public void setBillAddresCity(String billAddresCity) {
        BillAddresCity = billAddresCity;
    }

    public String getBillAddressState() {
        return BillAddressState;
    }

    public void setBillAddressState(String billAddressState) {
        BillAddressState = billAddressState;
    }

    public String getBillAddressCountry() {
        return BillAddressCountry;
    }

    public void setBillAddressCountry(String billAddressCountry) {
        BillAddressCountry = billAddressCountry;
    }

    public String getShipAddressCity() {
        return ShipAddressCity;
    }

    public void setShipAddressCity(String shipAddressCity) {
        ShipAddressCity = shipAddressCity;
    }

    public String getShipAddressState() {
        return ShipAddressState;
    }

    public void setShipAddressState(String shipAddressState) {
        ShipAddressState = shipAddressState;
    }

    public String getShipAddressCountry() {
        return ShipAddressCountry;
    }

    public void setShipAddressCountry(String shipAddressCountry) {
        ShipAddressCountry = shipAddressCountry;
    }

    public String getId_salesrep() {
        return Id_salesrep;
    }

    public void setId_salesrep(String id_salesrep) {
        Id_salesrep = id_salesrep;
    }

    public String getRouteDay1() {
        return RouteDay1;
    }

    public void setRouteDay1(String routeDay1) {
        RouteDay1 = routeDay1;
    }

    public String getRouteDay2() {
        return RouteDay2;
    }

    public void setRouteDay2(String routeDay2) {
        RouteDay2 = routeDay2;
    }

    public String getRouteDay3() {
        return RouteDay3;
    }

    public void setRouteDay3(String routeDay3) {
        RouteDay3 = routeDay3;
    }

    public String getRouteDay4() {
        return RouteDay4;
    }

    public void setRouteDay4(String routeDay4) {
        RouteDay4 = routeDay4;
    }

    public String getRouteDay5() {
        return RouteDay5;
    }

    public void setRouteDay5(String routeDay5) {
        RouteDay5 = routeDay5;
    }

    public String getRouteDay6() {
        return RouteDay6;
    }

    public void setRouteDay6(String routeDay6) {
        RouteDay6 = routeDay6;
    }

    public String getRouteDay7() {
        return RouteDay7;
    }

    public void setRouteDay7(String routeDay7) {
        RouteDay7 = routeDay7;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getBillAddress3() {
        return BillAddress3;
    }

    public void setBillAddress3(String billAddress3) {
        BillAddress3 = billAddress3;
    }

    public String getShipAddress3() {
        return ShipAddress3;
    }

    public void setShipAddress3(String shipAddress3) {
        ShipAddress3 = shipAddress3;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOtherDetails() {
        return OtherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        OtherDetails = otherDetails;
    }

    public String getId_term() {
        return Id_term;
    }

    public void setId_term(String id_term) {
        Id_term = id_term;
    }

    public String getZoeUpdateDate() {
        return ZoeUpdateDate;
    }

    public void setZoeUpdateDate(String zoeUpdateDate) {
        ZoeUpdateDate = zoeUpdateDate;
    }

    public String getZoeSyncDate() {
        return ZoeSyncDate;
    }

    public void setZoeSyncDate(String zoeSyncDate) {
        ZoeSyncDate = zoeSyncDate;
    }

    public Integer getNeedSync() {
        return NeedSync;
    }

    public void setNeedSync(Integer needSync) {
        NeedSync = needSync;
    }

    public String getPricelevel_ListID() {
        return Pricelevel_ListID;
    }

    public void setPricelevel_ListID(String pricelevel_ListID) {
        Pricelevel_ListID = pricelevel_ListID;
    }

    public String getOrigin() {
        return Origin;
    }

    public void setOrigin(String origin) {
        Origin = origin;
    }

    public String getVendor_ListID() {
        return Vendor_ListID;
    }

    public void setVendor_ListID(String vendor_ListID) {
        Vendor_ListID = vendor_ListID;
    }

    public String getEditSequence() {
        return EditSequence;
    }

    public void setEditSequence(String editSequence) {
        EditSequence = editSequence;
    }

    public Integer getNeedCorrection() {
        return NeedCorrection;
    }

    public void setNeedCorrection(Integer needCorrection) {
        NeedCorrection = needCorrection;
    }

    public Number getTotalSales() {
        return TotalSales;
    }

    public void setTotalSales(Number totalSales) {
        TotalSales = totalSales;
    }

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    private String ShipAddressZipcode;
    private String BillAddresZipCode;
    private String BillAddresCity;
    private String BillAddressState;
    private String BillAddressCountry;
    private String ShipAddressCity;
    private String ShipAddressState;
    private String ShipAddressCountry;
    private String Id_salesrep;
    private String RouteDay1;
    private String RouteDay2;
    private String RouteDay3;
    private String RouteDay4;
    private String RouteDay5;
    private String RouteDay6;
    private String RouteDay7;
    private String Fax;
    private String CompanyName;
    private String BillAddress3;
    private String ShipAddress3;
    private String Name;
    private String OtherDetails;
    private String Id_term;
    private String ZoeUpdateDate;
    private String ZoeSyncDate;
    private Integer NeedSync;
    private String Pricelevel_ListID;
    private String Origin;
    private String Vendor_ListID;
    private String EditSequence;
    private Integer NeedCorrection;

    private Number TotalSales;

    private String ListID;

    public static void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.Customer customerDAL = new com.pxelit.admin.zoeinventorymanagement.DAL.Customer();
        Customer customerBAL = new Customer();
        CustomerQueryRs customerQueryRs = response.QBXML.QBXMLMsgsRs.CustomerQueryRs;
        List<CustomerRet> customerRetList = customerQueryRs.CustomerRet;
        if (customerRetList == null)
            return;

        for (Iterator<CustomerRet> cr = customerRetList.iterator(); cr.hasNext(); ) {
            CustomerRet customerRet = cr.next();
            customerBAL.FullName = customerRet.get_FullName();
            customerBAL.ListID = customerRet.get_ListID();
            customerBAL.IsActive = customerRet.get_IsActive();
            customerBAL.BillAddress1 = customerRet.BillAddress.get_Addr1();
            customerBAL.BillAddress2 = customerRet.BillAddress.get_Addr2();
            customerBAL.ShipAddress1 = customerRet.shipAddress.get_Addr1();
            customerBAL.ShipAddress2 = customerRet.shipAddress.get_Addr2();
            customerBAL.OpenBalance = customerRet.get_Balance();
            customerBAL.OverdueBalance = customerRet.get_Balance();
            customerBAL.WorkPhone = customerRet.get_Phone();
            customerBAL.CellPhone = customerRet.get_Phone();
            customerBAL.Email = customerRet.get_Email();
            customerBAL.BillAddresZipCode = customerRet.BillAddress.get_PostalCode();
            customerBAL.ShipAddressZipcode = customerRet.shipAddress.get_PostalCode();
            customerBAL.Id_salesrep = customerRet.SalesRepRef.get_ListID();
//            String[] days = Util.ParseCustomerRouteDay(customerRet.DataExtRet.get(2).getDataExtValue());
//            customerBAL._routeDay3 = customerRet._routeDay3;
//            customerBAL._routeDay4 = customerRet._routeDay4;
//            customerBAL._routeDay5 = customerRet._routeDay5;
            customerBAL.Fax = customerRet.get_Fax();
            customerBAL.CompanyName = customerRet.get_CompanyName();
            customerDAL.Insert(ctx, customerBAL); //Save the data to the underline Data base
        }


    }

    public List<String> GetAll(Context ctx) {
        List<String> labels = new ArrayList<String>();
        com.pxelit.admin.zoeinventorymanagement.DAL.Customer customerDAL = new com.pxelit.admin.zoeinventorymanagement.DAL.Customer();
        Cursor cursor = customerDAL.GetAll(ctx);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return labels;
    }
}
