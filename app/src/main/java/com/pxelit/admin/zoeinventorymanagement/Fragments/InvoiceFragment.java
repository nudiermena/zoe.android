package com.pxelit.admin.zoeinventorymanagement.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.pxelit.admin.zoeinventorymanagement.BAL.Customer;
import com.pxelit.admin.zoeinventorymanagement.BAL.CustomerMsg;
import com.pxelit.admin.zoeinventorymanagement.BAL.Terms;
import com.pxelit.admin.zoeinventorymanagement.Common.AddProductDialogFragment;
import com.pxelit.admin.zoeinventorymanagement.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InvoiceFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InvoiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InvoiceFragment extends android.support.v4.app.Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Spinner sCustomer;
    private Spinner sTerms;
    private Spinner sCustomerMessage;
    private Button _btnAddItem;
    private FrameLayout _frameLayout;
    private LinearLayout _linearLayout;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    public InvoiceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InvoiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InvoiceFragment newInstance(String param1, String param2) {
        InvoiceFragment fragment = new InvoiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void ShowAddItemDialog(View view) {
        showDialog();
    }

    private void showDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        AddProductDialogFragment addProductDialogFragment = new AddProductDialogFragment();
        addProductDialogFragment.setCancelable(true);
        addProductDialogFragment.setDialogTitle("Enter Name");
        addProductDialogFragment.show(fragmentManager, "input dialog");
    }

    private void BindTerms(View view) {
        Terms terms = new Terms();
//        String[] from = {"_id", "name"};
//        int[] to = {android.R.id.text1};
        sTerms = (Spinner) view.findViewById(R.id.cboTerms);
        //Cursor cursor = terms.GetAll(view.getContext().getApplicationContext());
        List<String> lables = terms.GetAll(view.getContext().getApplicationContext());
        lables.add("Choose One...");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext().getApplicationContext(), android.R.layout.simple_spinner_item, lables);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sTerms.setAdapter(dataAdapter);
        sTerms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getSelectedItem().toString();
                Toast.makeText(view.getContext().getApplicationContext(), "You have selected:" + selectedItem, Toast.LENGTH_SHORT);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        if (cursor.getCount() > 0) {
//            SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(view.getContext().getApplicationContext(), android.R.layout.simple_spinner_item, cursor, from, to);
//            simpleCursorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            sTerms.setAdapter(simpleCursorAdapter);
//        }
    }

    private void BindCustomerMessage(View view) {
        CustomerMsg customerMsg = new CustomerMsg();
//        String[] from = {"_id", "name"};
//        int[] to = {android.R.id.text1};
        sCustomerMessage = (Spinner) view.findViewById(R.id.cboCustomerMsg);
        //Cursor cursor = terms.GetAll(view.getContext().getApplicationContext());
        List<String> lables = customerMsg.GetAll(view.getContext().getApplicationContext());
        //lables.add("Choose One...");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext().getApplicationContext(), android.R.layout.simple_spinner_item, lables);
        //dataAdapter.insert("Choose One...", -1);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sCustomerMessage.setAdapter(dataAdapter);
        sCustomerMessage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getSelectedItem().toString();
                Toast.makeText(view.getContext().getApplicationContext(), "You have selected:" + selectedItem, Toast.LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void BindCustomers(View view) {
        Customer customer = new Customer();
        sCustomer = (Spinner) view.findViewById(R.id.cboCustomer);
        List<String> lables = customer.GetAll(view.getContext().getApplicationContext());
        //lables.add("Choose One...");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(view.getContext().getApplicationContext(), android.R.layout.simple_spinner_item, lables);
        // dataAdapter.insert("Choose One...", -1);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sCustomer.setAdapter(dataAdapter);
        sCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getSelectedItem().toString();
                Toast.makeText(view.getContext().getApplicationContext(), "You have selected:" + selectedItem, Toast.LENGTH_SHORT);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    public void AddItemDinamically(View v) {
        this._frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
        this._linearLayout = (LinearLayout) v.findViewById(R.id.linearRoot);
        this._linearLayout.removeViewAt(8);

        this._btnAddItem = (Button) v.findViewById(R.id.btnAddItem);
        this._btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_frameLayout == null) {
                    _linearLayout.addView(_frameLayout, 8, new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT
                            , LinearLayoutCompat.LayoutParams.WRAP_CONTENT));
                } else {
                    _linearLayout.removeViewAt(8);
                }

                //TransitionManager.beginDelayedTransition(_linearLayout, new Slide());
                // ToggleView(_frameLayout);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.invoice_fragment, container, false);

        BindTerms(v);
        BindCustomerMessage(v);
        BindCustomers(v);

//        ViewParent viewParent = this._frameLayout.getParent();
//        Toast.makeText(v.getContext().getApplicationContext(), "ParentView is:" + viewParent.toString(), Toast.LENGTH_LONG).show();

        this._frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
        this._linearLayout = (LinearLayout) v.findViewById(R.id.linearRoot);


//        if (v.findViewById(R.id.frameAddItem) != null) {
//            this._frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
//            this._linearLayout.removeViewAt(8);
//        } else {
//            this._frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
//        }


        this._btnAddItem = (Button) v.findViewById(R.id.btnAddItem);
        this._btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_frameLayout.getParent() != null) {
                    ((ViewGroup) _frameLayout.getParent()).removeViewAt(8);
                } else {
                    ViewGroup parent = (ViewGroup) v.findViewById(R.id.linearRoot);
                    parent.addView(_frameLayout, 8, new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT
                            , LinearLayoutCompat.LayoutParams.WRAP_CONTENT));
                }


//                if (_frameLayout != null) {
//                    _linearLayout.removeViewAt(8);
//                    //_frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
//                } else {
//                    _linearLayout.addView(_frameLayout, 8, new LinearLayout.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT
//                            , LinearLayoutCompat.LayoutParams.WRAP_CONTENT));
//
//                    _frameLayout = (FrameLayout) v.findViewById(R.id.frameAddItem);
//
//                }

                //TransitionManager.beginDelayedTransition(_linearLayout, new Slide());
                // ToggleView(_frameLayout);
            }
        });

        return v;
    }

    private void ToggleView(View v) {
        boolean isVisible = v.getVisibility() == View.VISIBLE;
        v.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
