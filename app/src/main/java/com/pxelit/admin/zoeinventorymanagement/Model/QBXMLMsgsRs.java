package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 09/11/2016.
 */

public class QBXMLMsgsRs {
    public TermsQueryRs TermsQueryRs;
    public EmployeeQueryRs EmployeeQueryRs;
    public PriceLevelQueryRs PriceLevelQueryRs;
    public SalesTaxCodeQueryRs SalesTaxCodeQueryRs;
    public CreditMemoQueryRs CreditMemoQueryRs;
    public InventorySiteQueryRs InventorySiteQueryRs;
    public SalesRepQueryRs SalesRepQueryRs;
    public ClassQueryRs classQueryRs;
    public SalesTaxCodeQueryRs getSalesTaxCodeQueryRs;
    public VendorQueryRs vendorQueryRs;
    public PaymentMethodQueryRs paymentMethodQueryRs;
    public ItemQueryRs itemQueryRs;
    public InvoiceQueryRs invoiceQueryRs;
    public CreditMemoQueryRs creditMemoQueryRs;
    public CustomerMsgQueryRs CustomerMsgQueryRs;
    public CustomerQueryRs CustomerQueryRs;
}
