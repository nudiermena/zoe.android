package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

/**
 * Created by Admin on 12/12/2016.
 */

public class SalesTax {

    public static Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.SalesTax salesTaxBAL) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ListID", salesTaxBAL.getListID());
        contentValues.put("Name", salesTaxBAL.getName());
        contentValues.put("desc", salesTaxBAL.getDesc());

        Long rowsAffected = _dataAdapter.ExecuteNonQueryInsert(Constans.SALESTAX_TABLENAME, contentValues);
        _dataAdapter.close();
        return rowsAffected;
    }


    public static Cursor GetAll(Context ctx) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();
        //String[] columsToRetrive = {"ListID", "FullName"};
        String sql = "select ListID, Name, desc from salesTax";
        Cursor cursor = _dataAdapter.ExecuteDataRawQuery(sql, null);
//        if (cursor != null) {
//            if (cursor.moveToFirst()) {
//            }
//        }
        // _dataAdapter.close();
        return cursor;
    }
}
