package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

/**
 * Created by Admin on 23/11/2016.
 */

public class CustomerMsg {

    public static Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.CustomerMsg customerMsgBAL) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ListID", customerMsgBAL.getListID());
        contentValues.put("FullName", customerMsgBAL.getFullName());
        Long rowsAffected = _dataAdapter.ExecuteNonQueryInsert(Constans.CUSTOMERMSG_TABLENAME, contentValues);
        _dataAdapter.close();
        return rowsAffected;
    }


    public static Cursor GetAll(Context ctx) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();
        //String[] columsToRetrive = {"ListID", "FullName"};
        String sql = "select ListID, FullName from customer_msg";
        Cursor cursor = _dataAdapter.ExecuteDataRawQuery(sql, null);
//        if (cursor != null) {
//            if (cursor.moveToFirst()) {
//            }
//        }
        // _dataAdapter.close();
        return cursor;
    }
}
