package com.pxelit.admin.zoeinventorymanagement.Common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.pxelit.admin.zoeinventorymanagement.R;

/**
 * Created by Admin on 06/12/2016.
 */

public class AddProductDialogFragment extends android.support.v4.app.DialogFragment {

    Spinner _sProduct;
    EditText _txtQuantity;
    EditText _txtPrice;
    Button _btnAdd;
    Button _btnCancel;
    static String dialogTitle;

    public interface AddProductDialogListener {
        void onFinishInputDialog(String inputText);
    }

    public AddProductDialogFragment() {
        //---empty constructor required--
    }

    public void setDialogTitle(String title) {
        dialogTitle = title;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_item_fragment, container);
        this._sProduct = (Spinner) view.findViewById(R.id.cboProduct);
        this._txtQuantity = (EditText) view.findViewById(R.id.txtQuantity);
        this._txtPrice = (EditText) view.findViewById(R.id.txtRatePrice);
        this._btnAdd = (Button) view.findViewById(R.id.btnAddProduct);
        this._btnCancel = (Button) view.findViewById(R.id.btnCancel);
        this._btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddProductDialogListener activity = (AddProductDialogListener) getActivity();
                activity.onFinishInputDialog(_txtQuantity.getText().toString());
                activity.onFinishInputDialog(_txtPrice.getText().toString());
                activity.onFinishInputDialog(_sProduct.getSelectedItem().toString());
                dismiss();
            }
        });

        this._btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().setTitle("");
        return view;
    }
}
