package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 16/11/2016.
 */

public class ItemFixedAssetRet {

    public String get_ListID() {
        return _ListID;
    }

    public void set_ListID(String _ListID) {
        this._ListID = _ListID;
    }

    public String get_TimeCreated() {
        return _TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this._TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return _TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this._TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return _EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this._EditSequence = _EditSequence;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public Boolean get_IsActive() {
        return _IsActive;
    }

    public void set_IsActive(Boolean _IsActive) {
        this._IsActive = _IsActive;
    }

    public String get_AcquiredAs() {
        return _AcquiredAs;
    }

    public void set_AcquiredAs(String _AcquiredAs) {
        this._AcquiredAs = _AcquiredAs;
    }

    public String get_PurchaseDesc() {
        return _PurchaseDesc;
    }

    public void set_PurchaseDesc(String _PurchaseDesc) {
        this._PurchaseDesc = _PurchaseDesc;
    }

    public String get_PurchaseDate() {
        return _PurchaseDate;
    }

    public void set_PurchaseDate(String _PurchaseDate) {
        this._PurchaseDate = _PurchaseDate;
    }

    public String get_PurchaseCost() {
        return _PurchaseCost;
    }

    public void set_PurchaseCost(String _PurchaseCost) {
        this._PurchaseCost = _PurchaseCost;
    }

    public String get_VendorOrPayeeName() {
        return _VendorOrPayeeName;
    }

    public void set_VendorOrPayeeName(String _VendorOrPayeeName) {
        this._VendorOrPayeeName = _VendorOrPayeeName;
    }

    public String get_AssetDesc() {
        return _AssetDesc;
    }

    public void set_AssetDesc(String _AssetDesc) {
        this._AssetDesc = _AssetDesc;
    }

    public String get_PONumber() {
        return _PONumber;
    }

    public void set_PONumber(String _PONumber) {
        this._PONumber = _PONumber;
    }

    public String get_SerialNumber() {
        return _SerialNumber;
    }

    public void set_SerialNumber(String _SerialNumber) {
        this._SerialNumber = _SerialNumber;
    }

    private String _ListID;
    private String _TimeCreated;
    private String _TimeModified;
    private String _EditSequence;
    private String _Name;
    private Boolean _IsActive;
    private String _AcquiredAs;
    private String _PurchaseDesc;
    private String _PurchaseDate;
    private String _PurchaseCost;
    private String _VendorOrPayeeName;

    List<AssetAccountRef> AssetAccountRef;

    private String _AssetDesc;
    private String _PONumber;
    private String _SerialNumber;
}
