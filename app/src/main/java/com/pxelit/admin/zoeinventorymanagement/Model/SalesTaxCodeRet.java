package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 12/11/2016.
 */

public class SalesTaxCodeRet {

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getTimeCreated() {
        return TimeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        TimeCreated = timeCreated;
    }

    public String getTimeModified() {
        return TimeModified;
    }

    public void setTimeModified(String timeModified) {
        TimeModified = timeModified;
    }

    public String getEditSequence() {
        return EditSequence;
    }

    public void setEditSequence(String editSequence) {
        EditSequence = editSequence;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getActive() {
        return IsActive;
    }

    public void setActive(Boolean active) {
        IsActive = active;
    }

    public Boolean getTaxable() {
        return IsTaxable;
    }

    public void setTaxable(Boolean taxable) {
        IsTaxable = taxable;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }


    private String ListID;
    private String TimeCreated;
    private String TimeModified;
    private String EditSequence;
    private String Name;
    private Boolean IsActive;
    private Boolean IsTaxable;
    private String Desc;

}


