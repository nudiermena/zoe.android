package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 15/11/2016.
 */

public class ARAccountRef {
    public String get_ListID() {
        return _ListID;
    }

    public void set_ListID(String _ListID) {
        this._ListID = _ListID;
    }

    public String get_FullName() {
        return _FullName;
    }

    public void set_FullName(String _FullName) {
        this._FullName = _FullName;
    }

    private String _ListID;
    private String _FullName;
}
