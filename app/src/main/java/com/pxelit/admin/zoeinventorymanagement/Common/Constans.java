package com.pxelit.admin.zoeinventorymanagement.Common;

/**
 * Created by Admin on 16/10/2016.
 */

public final class Constans {

    public static String DBNAME = "Zoe";
    public static String SALESPREFNAME = "SalesRepUserName";
    //--URL Service to Sync the Entities ---
    public static String SYNCSERVICEURL = "http://174.71.200.174:54320/SyncService/";
    //--URL Service to Sync the Terms to QuickBooks ---
    public static String SYNCTERM = "{synch:{uploadOperations:'<TermsQueryRq  requestID=\"Term1\"><ActiveStatus>ActiveOnly</ActiveStatus></TermsQueryRq>',responseFormat:'JSON',cache:'term'}}";
    //--URL Service to Sync the Terms to QuickBooks ---
    public static String SYNCEMPLOYEE = "{synch:{uploadOperations:'<EmployeeQueryRq requestID=\"c1\"><OwnerID>0</OwnerID></EmployeeQueryRq>',responseFormat:'JSON',cache:'employee',xpathExp:'//EmployeeRet[Name=\"Rosalba Ortega\"]'}}";
    //--URL Service to Sync the Terms to QuickBooks ---
    public static String SYNCINVENTORYITEMS = "{synch:{uploadOperations:'<ItemQueryRq></ItemQueryRq>',responseFormat:'JSON',cache:'item'}}";
    //--URL Service to Sync the Terms to QuickBooks ---
    public static String SYNCTAXES = "{synch:{uploadOperations:'<SalesTaxCodeQueryRq  requestID=\"tax1\"><ActiveStatus>ActiveOnly</ActiveStatus></SalesTaxCodeQueryRq>',responseFormat:'JSON',cache:'tax'}}";
    //--URL Service to Sync the Terms to QuickBooks ---
    public static String SYNCPRICELEVELS = "{synch:{uploadOperations:'<PriceLevelQueryRq  requestID=\"pl\"></PriceLevelQueryRq>',responseFormat:'JSON',cache:'pricelevel'}}";

    public static String SYNCUSTOMER = "{synch:{uploadOperations:'<CustomerQueryRq  requestID=\"customer1\"></CustomerQueryRq>',responseFormat:'JSON',cache:'customer',xpathExp:'//CustomerRet'}}";

    public static String SYNCSALESREP = "{synch:{uploadOperations:'<SalesRepQueryRq requestID=\"c1\"></SalesRepQueryRq>',responseFormat:'JSON',cache:'salesrep',xpathExp:'//SalesRepRet[SalesRepEntityRef/FullName=\"Rosalba Ortega\"]'}}";

    public static String SYNCVENDORS = "{synch:{uploadOperations:'<VendorQueryRq  requestID=\"vendor1\"></VendorQueryRq>',responseFormat:'JSON',cache:'vendor',xpathExp:'//VendorRet'}}";

    public static String SYNCPAYMENTSMETHODS = "{synch:{uploadOperations:'<PaymentMethodQueryRq  requestID=\"PaymentMethod1\"><ActiveStatus>ActiveOnly</ActiveStatus></PaymentMethodQueryRq>',responseFormat:'JSON',cache:'paymentmethod'}}";

    public static String SYNCCUSTOMERMSG = "{synch:{uploadOperations:'<CustomerMsgQueryRq  requestID=\"customermsg1\"></CustomerMsgQueryRq>',responseFormat:'JSON',cache:'customerMsg'}}";

    public static String SYNCCREDITMEMO = " {synch:{uploadOperations:'<CreditMemoQueryRq requestID=\"creditMemo001\"></CreditMemoQueryRq>',responseFormat:'JSON',cache:'creditMemo',xpathExp:'//CreditMemoRet'}}";

    public static String SYNCINVENTORYSITE = "{synch:{uploadOperations:'<InventorySiteQueryRq  requestID=\"InventorySite1\"><ActiveStatus>ActiveOnly</ActiveStatus></InventorySiteQueryRq>',responseFormat:'JSON',cache:'inventorysite'}}";

    public static String SYNCSITEITEMS = " {synch:{uploadOperations:'<ItemQueryRq></ItemQueryRq>',responseFormat:'JSON',cache:'item'}}";

    public static String SYNCINVOICE = "{synch:{uploadOperations:'<InvoiceQueryRq requestID=\"invoice\"></InvoiceQueryRq>',responseFormat:'JSON',cache:'invoice',xpathExp:'//InvoiceRet'}}";


    public static String SALESREPTABLENAME = "salesrep";
    public static String CUSTOMERTABLENAME = "customer";
    public static String INVOICETABLENAME = "invoice";
    public static String INVOICE_ITEMTABLENAME = "invoice_item";
    public static String TERM_TABLENAME = "term";
    public static String PRICELEVEL_TABLENAME = "term";
    public static String CUSTOMERMSG_TABLENAME = "customer_msg";
    public static String SALESTAX_TABLENAME = "salesTax";


}
