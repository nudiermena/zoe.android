package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Model.DataExtRet;
import com.pxelit.admin.zoeinventorymanagement.Model.EmployeeRet;
import com.pxelit.admin.zoeinventorymanagement.Model.Response;
import com.pxelit.admin.zoeinventorymanagement.Model.SalesRepRet;

import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 17/10/2016.
 */

public class SalesRep {

    public String get_salesrepID() {
        return _salesrepID;
    }

    public void set_salesrepID(String _salesrepID) {
        this._salesrepID = _salesrepID;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public Boolean get_isActive() {
        return _isActive;
    }

    public void set_isActive(Boolean _isActive) {
        this._isActive = _isActive;
    }

    public String get_syncTime() {
        return new Date().toString();
    }


    public String get_initial() {
        return _initial;
    }

    public void set_initial(String _initial) {
        this._initial = _initial;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String get_roles() {
        return _roles;
    }

    public void set_roles(String _roles) {
        this._roles = _roles;
    }

    private String _salesrepID;
    private String _name;
    private String _password;
    private Boolean _isActive;
    private String _initial;
    private String employeeID;
    private String _roles;

    public SalesRep() {

    }

    public static void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep();
        SalesRep salesBAL = new SalesRep();
        EmployeeRet employeeRet = response.QBXML.QBXMLMsgsRs.EmployeeQueryRs.EmployeeRet;
        List<DataExtRet> dataExtRetList = employeeRet.DataExtRet;
        if (dataExtRetList == null)
            return;

        DataExtRet dataExtRet = dataExtRetList.get(0);
        if (dataExtRet.getDataExtName().equals("Password")) {
            salesBAL._password = dataExtRet.getDataExtValue();
        }
        salesBAL._name = employeeRet.getName();
        salesBAL._isActive = employeeRet.getActive();
        salesRep.Insert(ctx, salesBAL); //Save the data to the underline Data base
    }

    public static void UpdateQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep();
        SalesRep salesBAL = new SalesRep();
        SalesRepRet salesRepRet = response.QBXML.QBXMLMsgsRs.SalesRepQueryRs.SalesRepRet;
        if (salesRepRet == null)
            return;

        salesRep.Update(ctx, salesBAL); //Save the data to the underline Data base
    }

    public boolean Exists(Context ctx, SalesRep salesRepBAL) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep();
        Cursor cursor = salesRep.Exists(ctx, salesRepBAL.get_name().trim(), get_password().trim());
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                return true;
            }
        }
        cursor.close();
        return false;
    }

}
