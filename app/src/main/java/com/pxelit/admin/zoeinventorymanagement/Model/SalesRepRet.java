package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 19/11/2016.
 */

public class SalesRepRet {
    String ListID;

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getTimeCreated() {
        return TimeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        TimeCreated = timeCreated;
    }

    public String getTimeModified() {
        return TimeModified;
    }

    public void setTimeModified(String timeModified) {
        TimeModified = timeModified;
    }

    public String getEditSequence() {
        return EditSequence;
    }

    public void setEditSequence(String editSequence) {
        EditSequence = editSequence;
    }

    public String getInitial() {
        return Initial;
    }

    public void setInitial(String initial) {
        Initial = initial;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    String TimeCreated;
    String TimeModified;
    String EditSequence;
    String Initial;
    String IsActive;
    public SalesRepEntityRef salesRepEntityRef;
}

