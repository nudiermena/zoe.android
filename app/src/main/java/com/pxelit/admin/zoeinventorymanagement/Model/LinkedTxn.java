package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 15/11/2016.
 */

public class LinkedTxn {
    public String get_TxnID() {
        return _TxnID;
    }

    public void set_TxnID(String _TxnID) {
        this._TxnID = _TxnID;
    }

    public String get_TxnType() {
        return _TxnType;
    }

    public void set_TxnType(String _TxnType) {
        this._TxnType = _TxnType;
    }

    public String get_TxnDate() {
        return _TxnDate;
    }

    public void set_TxnDate(String _TxnDate) {
        this._TxnDate = _TxnDate;
    }

    public String get_RefNumber() {
        return _RefNumber;
    }

    public void set_RefNumber(String _RefNumber) {
        this._RefNumber = _RefNumber;
    }

    public String get_LinkType() {
        return _LinkType;
    }

    public void set_LinkType(String _LinkType) {
        this._LinkType = _LinkType;
    }

    public String get_Amount() {
        return _Amount;
    }

    public void set_Amount(String _Amount) {
        this._Amount = _Amount;
    }


    private String _TxnID;
    private String _TxnType;
    private String _TxnDate;
    private String _RefNumber;
    private String _LinkType;
    private String _Amount;
}
