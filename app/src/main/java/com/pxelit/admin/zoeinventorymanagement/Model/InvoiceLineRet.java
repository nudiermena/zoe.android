package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 19/11/2016.
 */

public class InvoiceLineRet {

    public String get_TxnLineID() {
        return _TxnLineID;
    }

    public void set_TxnLineID(String _TxnLineID) {
        this._TxnLineID = _TxnLineID;
    }

    public String get_Desc() {
        return _Desc;
    }

    public void set_Desc(String _Desc) {
        this._Desc = _Desc;
    }

    public String get_Quantity() {
        return _Quantity;
    }

    public void set_Quantity(String _Quantity) {
        this._Quantity = _Quantity;
    }

    public String get_Rate() {
        return _Rate;
    }

    public void set_Rate(String _Rate) {
        this._Rate = _Rate;
    }

    public String get_Amount() {
        return _Amount;
    }

    public void set_Amount(String _Amount) {
        this._Amount = _Amount;
    }


    private String _TxnLineID;

    List<ItemRef> ItemRef;

    private String _Desc;
    private String _Quantity;
    private String _Rate;
    private String _Amount;

    List<InventorySiteRef> InventorySiteRef;
    List<SalesTaxCodeRef> SalesTaxCodeRef;
}
