package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 19/11/2016.
 */

public class SalesRepEntityRef {

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    String ListID;
    String FullName;

}
