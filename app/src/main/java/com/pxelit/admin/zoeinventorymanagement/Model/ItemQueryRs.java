package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 16/11/2016.
 */

public class ItemQueryRs {

    public String get_statusCode() {
        return _statusCode;
    }

    public void set_statusCode(String _statusCode) {
        this._statusCode = _statusCode;
    }

    public String get_statusSeverity() {
        return _statusSeverity;
    }

    public void set_statusSeverity(String _statusSeverity) {
        this._statusSeverity = _statusSeverity;
    }

    public String get_statusMessage() {
        return _statusMessage;
    }

    public void set_statusMessage(String _statusMessage) {
        this._statusMessage = _statusMessage;
    }


    private String _statusCode;
    private String _statusSeverity;
    private String _statusMessage;

    List<ItemServiceRet> ItemServiceRet;
    List<ItemNonInventoryRet> ItemNonInventoryRet;
    List<ItemOtherChargeRet> ItemOtherChargeRet;
    List<ItemInventoryRet> ItemInventoryRet;
    List<ItemFixedAssetRet> ItemFixedAssetRet;
    List<ItemDiscountRet> ItemDiscountRet;
}
