package com.pxelit.admin.zoeinventorymanagement.Common;

/**
 * Created by Admin on 20/11/2016.
 */

public class Util {
    Day _day;

    public static String[] ParseCustomerRouteDay(String routes) {
        return routes.split(",");
    }

    public Util(Day day) {
        this._day = day;
    }

    public enum Day {
        Sun, Mon, Tue, Wed,
        Thu, Fri, Sat
    }
}
