package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 10/11/2016.
 */

public class DataExtRet {
    Integer OwnerID;
    String DataExtName;

    public Integer getOwnerID() {
        return OwnerID;
    }

    public void setOwnerID(Integer ownerID) {
        OwnerID = ownerID;
    }

    public String getDataExtName() {
        return DataExtName;
    }

    public void setDataExtName(String dataExtName) {
        DataExtName = dataExtName;
    }

    public String getDataExtType() {
        return DataExtType;
    }

    public void setDataExtType(String dataExtType) {
        DataExtType = dataExtType;
    }

    public String getDataExtValue() {
        return DataExtValue;
    }

    public void setDataExtValue(String dataExtValue) {
        DataExtValue = dataExtValue;
    }

    String DataExtType;
    String DataExtValue;
}
