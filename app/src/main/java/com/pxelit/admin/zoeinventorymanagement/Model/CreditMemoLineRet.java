package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 15/11/2016.
 */

public class CreditMemoLineRet {

    public String get_TxnLineID() {
        return _TxnLineID;
    }

    public void set_TxnLineID(String _TxnLineID) {
        this._TxnLineID = _TxnLineID;
    }

    public String get_Desc() {
        return _Desc;
    }

    public void set_Desc(String _Desc) {
        this._Desc = _Desc;
    }

    public Integer get_Quantity() {
        return _Quantity;
    }

    public void set_Quantity(Integer _Quantity) {
        this._Quantity = _Quantity;
    }

    public String get_Rate() {
        return _Rate;
    }

    public void set_Rate(String _Rate) {
        this._Rate = _Rate;
    }

    public String get_Amount() {
        return _Amount;
    }

    public void set_Amount(String _Amount) {
        this._Amount = _Amount;
    }

    private String _TxnLineID;
    public ItemRef ItemRef;
    private String _Desc;
    private Integer _Quantity;
    private String _Rate;
    public ClassRef ClassRef;
    private String _Amount;
    public InventorySiteRef InventorySiteRef;
    public SalesTaxCodeRef SalesTaxCodeRef;
}
