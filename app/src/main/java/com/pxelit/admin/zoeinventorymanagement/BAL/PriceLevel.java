package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;

import com.pxelit.admin.zoeinventorymanagement.Model.DataExtRet;
import com.pxelit.admin.zoeinventorymanagement.Model.EmployeeRet;
import com.pxelit.admin.zoeinventorymanagement.Model.PriceLevelQueryRs;
import com.pxelit.admin.zoeinventorymanagement.Model.PriceLevelRet;
import com.pxelit.admin.zoeinventorymanagement.Model.Response;
import com.pxelit.admin.zoeinventorymanagement.Model.StandardTermsRet;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class PriceLevel {
    private String ListID;
    private String name;
    private String type;

    public String getFixedPercentage() {
        return fixedPercentage;
    }

    public void setFixedPercentage(String fixedPercentage) {
        this.fixedPercentage = fixedPercentage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    private String fixedPercentage;

    public static void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep();
        PriceLevel _priceLevel = new PriceLevel();
        PriceLevelQueryRs priceLevelQueryRs = response.QBXML.QBXMLMsgsRs.PriceLevelQueryRs;
        if (priceLevelQueryRs == null)
            return;

        List<PriceLevelRet> priceLevelRetList = priceLevelQueryRs.PriceLevelRet;
        for (Iterator<PriceLevelRet> p = priceLevelRetList.iterator(); p.hasNext(); ) {
            PriceLevelRet priceLevelRet = p.next();
            _priceLevel.ListID = priceLevelRet.get_ListID();
            _priceLevel.name = priceLevelRet.get_Name();
            _priceLevel.fixedPercentage = priceLevelRet.get_fixedPercentage();
            com.pxelit.admin.zoeinventorymanagement.DAL.PriceLevel.Insert(ctx, _priceLevel);
        }
    }
}
