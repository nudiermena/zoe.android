package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Model.Response;
import com.pxelit.admin.zoeinventorymanagement.Model.SalesTaxCodeRet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 12/12/2016.
 */

public class SalesTax {


    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    private String ListID;
    private String Name;
    private Boolean IsActive;
    private Boolean IsTaxable;
    private String Desc;

    public void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesTax salesTax = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesTax();
        SalesTax salesTaxBAL = new SalesTax();
        SalesTaxCodeRet _salesTaxCodeQueryRs;

        List<SalesTaxCodeRet> salesTaxCodeRets = response.QBXML.QBXMLMsgsRs.SalesTaxCodeQueryRs.SalesTaxCodeRet;
        if (salesTaxCodeRets == null)
            return;

        for (Iterator<SalesTaxCodeRet> salesT = salesTaxCodeRets.iterator(); salesT.hasNext(); ) {
            _salesTaxCodeQueryRs = salesT.next();

            salesTaxBAL.ListID = _salesTaxCodeQueryRs.getListID();
            salesTaxBAL.Name = _salesTaxCodeQueryRs.getName();
            salesTaxBAL.Desc = _salesTaxCodeQueryRs.getDesc();
            com.pxelit.admin.zoeinventorymanagement.DAL.SalesTax.Insert(ctx, salesTaxBAL);
        }
    }

    public List<String> GetAll(Context ctx) {
        List<String> labels = new ArrayList<String>();
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesTax salesTaxDAL = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesTax();
        Cursor cursor = salesTaxDAL.GetAll(ctx);
        if (!cursor.isClosed()) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return labels;
    }

}
