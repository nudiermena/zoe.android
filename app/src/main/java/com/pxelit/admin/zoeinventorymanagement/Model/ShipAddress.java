package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 16/11/2016.
 */

public class ShipAddress {

    public String get_Addr1() {
        return _Addr1;
    }

    public void set_Addr1(String _Addr1) {
        this._Addr1 = _Addr1;
    }

    public String get_Addr2() {
        return _Addr2;
    }

    public void set_Addr2(String _Addr2) {
        this._Addr2 = _Addr2;
    }

    public String get_City() {
        return _City;
    }

    public void set_City(String _City) {
        this._City = _City;
    }

    public String get_State() {
        return _State;
    }

    public void set_State(String _State) {
        this._State = _State;
    }

    public String get_PostalCode() {
        return _PostalCode;
    }

    public void set_PostalCode(String _PostalCode) {
        this._PostalCode = _PostalCode;
    }

    private String _Addr1;
    private String _Addr2;
    private String _City;
    private String _State;
    private String _PostalCode;
}
