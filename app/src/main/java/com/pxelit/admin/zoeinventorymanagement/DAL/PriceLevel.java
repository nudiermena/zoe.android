package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

/**
 * Created by Admin on 12/11/2016.
 */

public class PriceLevel {

    public static Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.PriceLevel priceLevelBAL) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ListID", priceLevelBAL.getListID());
        contentValues.put("name", priceLevelBAL.getName());
        //contentValues.put("type", priceLevelBAL.getType());
        contentValues.put("fixedPercentage", priceLevelBAL.getFixedPercentage());
        Long rowsAffected = _dataAdapter.ExecuteNonQueryInsert(Constans.PRICELEVEL_TABLENAME, contentValues);
        _dataAdapter.close();
        return rowsAffected;
    }

    public static Cursor GetAll(Context ctx) {
        DataAdapter _dataAdapter = new DataAdapter(ctx);
        _dataAdapter.open();
        String[] columsToRetrive = {"ListID", "name", "type", "fixedPercentage"};
        Cursor cursor = _dataAdapter.ExecuteDataDatable(Constans.PRICELEVEL_TABLENAME, columsToRetrive, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
            }
        }
        _dataAdapter.close();
        return cursor;
    }
}
