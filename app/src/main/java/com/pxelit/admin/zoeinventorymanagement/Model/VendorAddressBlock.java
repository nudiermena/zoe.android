package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 12/11/2016.
 */

public class VendorAddressBlock {

    public String get_Addr1() {
        return _Addr1;
    }

    public void set_Addr1(String _Addr1) {
        this._Addr1 = _Addr1;
    }

    public String get_Addr2() {
        return _Addr2;
    }

    public void set_Addr2(String _Addr2) {
        this._Addr2 = _Addr2;
    }

    public String get_Addr3() {
        return _Addr3;
    }

    public void set_Addr3(String _Addr3) {
        this._Addr3 = _Addr3;
    }


    private String _Addr1;
    private String _Addr2;
    private String _Addr3;
}
