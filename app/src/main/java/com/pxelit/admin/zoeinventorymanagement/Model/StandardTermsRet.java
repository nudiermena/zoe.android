package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.Date;


public class StandardTermsRet {

    private String ListID;

    public String getListID() {
        return ListID;
    }

    public void setListID(String listID) {
        ListID = listID;
    }

    public Date getTimeCreated() {
        return TimeCreated;
    }

    public void setTimeCreated(Date timeCreated) {
        TimeCreated = timeCreated;
    }

    public Date getTimeModified() {
        return TimeModified;
    }

    public void setTimeModified(Date timeModified) {
        TimeModified = timeModified;
    }

    public Integer getEditSequence() {
        return EditSequence;
    }

    public void setEditSequence(Integer editSequence) {
        EditSequence = editSequence;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Boolean getSsActive() {
        return IsActive;
    }

    public void setSsActive(Boolean ssActive) {
        IsActive = ssActive;
    }

    public Integer getStdDueDays() {
        return StdDueDays;
    }

    public void setStdDueDays(Integer stdDueDays) {
        StdDueDays = stdDueDays;
    }

    public Integer getStdDiscountDays() {
        return StdDiscountDays;
    }

    public void setStdDiscountDays(Integer stdDiscountDays) {
        StdDiscountDays = stdDiscountDays;
    }

    public String getDiscountPct() {
        return DiscountPct;
    }

    public void setDiscountPct(String discountPct) {
        DiscountPct = discountPct;
    }

    private Date TimeCreated;
    private Date TimeModified;
    private Integer EditSequence;
    private String Name;
    private Boolean IsActive;
    private Integer StdDueDays;
    private Integer StdDiscountDays;
    private String DiscountPct;

}
