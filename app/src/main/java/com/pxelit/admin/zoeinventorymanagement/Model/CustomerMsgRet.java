package com.pxelit.admin.zoeinventorymanagement.Model;

/**
 * Created by Admin on 12/11/2016.
 */

public class CustomerMsgRet {

    public String get_ListID() {
        return ListID;
    }

    public void set_ListID(String _ListID) {
        this.ListID = _ListID;
    }

    public String get_TimeCreated() {
        return TimeCreated;
    }

    public void set_TimeCreated(String _TimeCreated) {
        this.TimeCreated = _TimeCreated;
    }

    public String get_TimeModified() {
        return TimeModified;
    }

    public void set_TimeModified(String _TimeModified) {
        this.TimeModified = _TimeModified;
    }

    public String get_EditSequence() {
        return EditSequence;
    }

    public void set_EditSequence(String _EditSequence) {
        this.EditSequence = _EditSequence;
    }

    public String get_Name() {
        return Name;
    }

    public void set_Name(String _Name) {
        this.Name = _Name;
    }

    public String get_IsActive() {
        return IsActive;
    }

    public void set_IsActive(String _IsActive) {
        this.IsActive = _IsActive;
    }

    String ListID;
    String TimeCreated;
    String TimeModified;
    String EditSequence;
    String Name;
    String IsActive;
}
