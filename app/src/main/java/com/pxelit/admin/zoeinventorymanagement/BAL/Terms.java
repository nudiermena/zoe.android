package com.pxelit.admin.zoeinventorymanagement.BAL;

import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Model.Response;
import com.pxelit.admin.zoeinventorymanagement.Model.StandardTermsRet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class Terms {

    String _ID;
    String _name;

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public int get_stdDueDay() {
        return _stdDueDay;
    }

    public void set_stdDueDay(int _stdDueDay) {
        this._stdDueDay = _stdDueDay;
    }

    public int get_stdDiscountDays() {
        return _stdDiscountDays;
    }

    public void set_stdDiscountDays(int _stdDiscountDays) {
        this._stdDiscountDays = _stdDiscountDays;
    }

    public String get_discountPct() {
        return _discountPct;
    }

    public void set_discountPct(String _discountPct) {
        this._discountPct = _discountPct;
    }

    int _stdDueDay;
    int _stdDiscountDays;
    String _discountPct;

    public void InsertarQBXML(Context ctx, Response response) {
        com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep salesRep = new com.pxelit.admin.zoeinventorymanagement.DAL.SalesRep();
        Terms termsBAL = new Terms();
        List<StandardTermsRet> standardTermsRetList = response.QBXML.QBXMLMsgsRs.TermsQueryRs.StandardTermsRet;
        StandardTermsRet _standardTermsRet;
        if (standardTermsRetList == null)
            return;

        for (Iterator<StandardTermsRet> s = standardTermsRetList.iterator(); s.hasNext(); ) {
            _standardTermsRet = s.next();
            termsBAL._name = _standardTermsRet.getName();
            termsBAL._stdDueDay = _standardTermsRet.getStdDueDays();
            termsBAL._stdDiscountDays = _standardTermsRet.getStdDueDays();
            termsBAL._discountPct = _standardTermsRet.getDiscountPct();
            com.pxelit.admin.zoeinventorymanagement.DAL.Terms.Insert(ctx, termsBAL);
        }
    }

    public List<String> GetAll(Context ctx) {
        List<String> labels = new ArrayList<String>();
        com.pxelit.admin.zoeinventorymanagement.DAL.Terms termsDAL = new com.pxelit.admin.zoeinventorymanagement.DAL.Terms();

        Cursor cursor = termsDAL.GetAll(ctx);
        if (!cursor.isClosed()) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(1));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return labels;
    }
}
