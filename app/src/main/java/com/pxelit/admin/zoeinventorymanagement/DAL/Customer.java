package com.pxelit.admin.zoeinventorymanagement.DAL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pxelit.admin.zoeinventorymanagement.Common.Constans;

/**
 * Created by Admin on 19/11/2016.
 */

public class Customer {

    DataAdapter _dataAdapter;

    public Long Insert(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.Customer customerBAL) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ListID", customerBAL.getListID());
        contentValues.put("FullName", customerBAL.getFullName());
        contentValues.put("IsActive", customerBAL.getIsActive());
        contentValues.put("billAddress1", customerBAL.getBillAddress3());
        contentValues.put("billAddress2", customerBAL.getBillAddress3());
        contentValues.put("shipAddress1", customerBAL.getShipAddress3());
        contentValues.put("shipAddress2", customerBAL.getShipAddress3());
        contentValues.put("openBalance", customerBAL.getOpenBalance().toString());
        contentValues.put("overdueBalance", customerBAL.getOpenBalance().toString());
        contentValues.put("workPhone", customerBAL.getWorkPhone());
        contentValues.put("cellPhone", customerBAL.getCellPhone());
        contentValues.put("email", customerBAL.getEmail());
        contentValues.put("shipAddressZipcode", customerBAL.getShipAddressZipcode());
        contentValues.put("billAddresZipCode", customerBAL.getBillAddresZipCode());

        Long rowsAffected = this._dataAdapter.ExecuteNonQueryInsert(Constans.SALESREPTABLENAME, contentValues);
        this._dataAdapter.close();
        return rowsAffected;
    }

    public Integer Update(Context ctx, com.pxelit.admin.zoeinventorymanagement.BAL.Customer customerBAL) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String query = "Name = " + customerBAL.getFullName();
        ContentValues contentValues = new ContentValues();
        Integer rowsAffected = this._dataAdapter.ExecuteNonQueryUpdate(Constans.SALESREPTABLENAME, contentValues, query);
        this._dataAdapter.close();
        return rowsAffected;
    }

    public Cursor Exists(Context ctx, String userName, String password) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String[] columsToRetrive = {"ListID",
                "FullName",
                "IsActive",
                "billAddress1",
                "billAddress2",
                "shipAddress1",
                "shipAddress2",
                "openBalance",
                "overdueBalance",
                "workPhone",
                "cellPhone",
                "email",
                "shipAddressZipcode",
                "billAddresZipCode"};

        String query = "Name =" + "'" + userName + "'" + " and " + "Password =" + "'" + password + "'";
        Cursor cursor = this._dataAdapter.ExecuteDataDatable(Constans.SALESREPTABLENAME, columsToRetrive, query);
        //this._dataAdapter.close();
        return cursor;

    }


    public Cursor GetAll(Context ctx) {
        this._dataAdapter = new DataAdapter(ctx);
        this._dataAdapter.open();
        String[] columsToRetrive = {"ListID",
                "FullName",
                "IsActive",
                "billAddress1",
                "billAddress2",
                "shipAddress1",
                "shipAddress2",
                "openBalance",
                "overdueBalance",
                "workPhone",
                "cellPhone",
                "email",
                "shipAddressZipcode",
                "billAddresZipCode"};
        Cursor cursor = this._dataAdapter.ExecuteDataDatable(Constans.CUSTOMERTABLENAME, columsToRetrive, null);
        return cursor;
    }
}
