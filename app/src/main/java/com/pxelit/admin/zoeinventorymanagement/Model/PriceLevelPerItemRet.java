package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.List;

/**
 * Created by Admin on 12/11/2016.
 */

public class PriceLevelPerItemRet {
    public Integer get_CustomPrice() {
        return _CustomPrice;
    }

    public void set_CustomPrice(Integer _CustomPrice) {
        this._CustomPrice = _CustomPrice;
    }

    List<ItemRef> ItemRef;
    private Integer _CustomPrice;

}
