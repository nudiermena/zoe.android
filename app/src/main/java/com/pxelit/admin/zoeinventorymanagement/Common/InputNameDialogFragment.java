package com.pxelit.admin.zoeinventorymanagement.Common;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.pxelit.admin.zoeinventorymanagement.R;

/**
 * Created by Admin on 06/11/2016.
 */

public class InputNameDialogFragment extends DialogFragment {
    EditText _txtConfirm;
    Button _btnConfirm;
    static String dialogTitle;

    public interface InputNameDialogListener {
        void onFinishInputDialog(String inputText);
    }

    public InputNameDialogFragment() {
        //---empty constructor required--
    }

    public void setDialogTitle(String title) {
        dialogTitle = title;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.confirm_fragment, container);
        this._txtConfirm = (EditText) view.findViewById(R.id.txtConfirm);
        this._btnConfirm = (Button) view.findViewById(R.id.btnConfirm);
        this._btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputNameDialogListener activity = (InputNameDialogListener) getActivity();
                activity.onFinishInputDialog(_txtConfirm.getText().toString());
                dismiss();
            }
        });

        _txtConfirm.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().setTitle("");
        return view;
    }
}
