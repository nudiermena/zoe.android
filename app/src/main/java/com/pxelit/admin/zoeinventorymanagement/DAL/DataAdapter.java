package com.pxelit.admin.zoeinventorymanagement.DAL;

/**
 * Created by Admin on 06/11/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Admin on 26/04/2016.
 */
public class DataAdapter {

    static final String DATABASE_NAME = "zoe";
    static final int DATABASE_VERSION = 1;

    final Context context;
    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public DataAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
//            try {
//                db.execSQL(DATABASE_CREATE);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("TAG", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS contacts");
            //onCreate(db);
        }
    }

    //---opens the database--
    public DataAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database--
    public void close() {
        DBHelper.close();
    }


    public Cursor ExecuteDataDatable(String tableName, String[] columnsToRetrive) {
        return db.query(tableName, columnsToRetrive, null, null, null, null, null);
    }

    public Cursor ExecuteDataDatable(String tableName, String[] columsToRetrive, String query) {
        Cursor mCursor = db.query(true, tableName, columsToRetrive, query, null, null, null, null, null);
        return mCursor;
    }

    public Cursor ExecuteDataRawQuery(String sql, String[] selectionArgs) {
        Cursor mCursor = db.rawQuery(sql, selectionArgs);
        return mCursor;
    }

    public Integer ExecuteNonQueryUpdate(String tableName, ContentValues contentValues, String where) {
        return db.update(tableName, contentValues, where, null);
    }

    public Long ExecuteNonQueryInsert(String tableName, ContentValues contentValues) {
        return db.insert(tableName, null, contentValues);
    }
}
