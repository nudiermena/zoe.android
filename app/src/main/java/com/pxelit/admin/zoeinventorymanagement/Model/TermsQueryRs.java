package com.pxelit.admin.zoeinventorymanagement.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 09/11/2016.
 */

public class TermsQueryRs {

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusSeverity() {
        return statusSeverity;
    }

    public void setStatusSeverity(String statusSeverity) {
        this.statusSeverity = statusSeverity;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    private String requestID;
    private int statusCode;
    private String statusSeverity;
    private String statusMessage;
    public List<StandardTermsRet> StandardTermsRet;
}
